﻿using System;
using System.Windows.Forms;

namespace Pixel
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            int caseSwitch;
            int aux = 1;
            do
            {
                Console.Write("\t \t Escolha entre gerar Pixel ou Reta \n" +
                    "\t [1] - Pixel \n" +
                    "\t [2] - Reta \n" +
                    "\t [0] - Sair \n" +
                    "\t => ");
                caseSwitch = Convert.ToInt32(Console.ReadLine());
                switch (caseSwitch)
                {
                    case 1:
                        //Ponto do pixel
                        Transformacao t = new Transformacao();
                        // X maximo
                        Console.Write("Digite o x Máximo: ");
                        double xMax = Convert.ToDouble(Console.ReadLine());
                        //X minimo
                        Console.Write("Digite o x Mínimo: ");
                        double xMin = Convert.ToDouble(Console.ReadLine());
                        Console.Write("Digite o y Máximo: ");
                        double yMax = Convert.ToDouble(Console.ReadLine());
                        Console.Write("Digite o y Mínimo: ");
                        double yMin = Convert.ToDouble(Console.ReadLine());

                        Console.Write("Digite o px: ");
                        double px = Convert.ToDouble(Console.ReadLine());
                        Console.Write("Digite o py: ");
                        double py = Convert.ToDouble(Console.ReadLine());


                        double[] traM = t.TransMundoNDCs(px, py, xMax, xMin, yMax, yMin);
                        Console.WriteLine("Ndcx = {0}\nNdcy = {1}", traM[0], traM[1]);
                        int[] traDc = t.NdcsToDcs(traM[0], traM[1], 571, 479);
                        Console.WriteLine("Dcx = {0}\nDcy = {1}", traDc[0], traDc[1]);
                        Console.ReadKey();

                        Application.EnableVisualStyles();
                        Application.Run(new Form1(traDc[0], traDc[1], caseSwitch, 0, 0));
                        break;
                    case 2:
                        //--------------Ponto 1---------------
                        Console.WriteLine("Ponto P1: ");
                        Console.Write("Digite seu x: ");
                        int x1 = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Digite seu y: ");
                        int y1 = Convert.ToInt32(Console.ReadLine());

                        //--------------Ponto 2---------------
                        Console.WriteLine("Ponto P2: ");
                        Console.Write("Digite seu x: ");
                        int x2 = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Digite seu y: ");
                        int y2 = Convert.ToInt32(Console.ReadLine());

                        Application.EnableVisualStyles();
                        Application.Run(new Form1(x1, y1, caseSwitch, x2, y2));
                        break;
                    default:
                        Console.Write("\t \t Deseja realmente sair? \n" +
                            "\t [1] - Não \n" +
                            "\t [0] - Sim" +
                            "\t => ");
                        aux = Convert.ToInt32(Console.ReadLine());
                        break;
                }
                if (aux != 0)
                {
                    Console.Write("\t \t Deseja repetir? \n" +
                            "\t [1] - Sim \n" +
                            "\t [0] - Não \n" +
                            "\t => ");
                    aux = Convert.ToInt32(Console.ReadLine());
                }

            } while (aux != 0);
            Console.ReadKey();

            

            
        }

        
    }
}
