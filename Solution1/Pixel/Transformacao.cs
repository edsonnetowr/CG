﻿using System;
namespace Pixel
{
    public class Transformacao
    {
        public double[] TransMundoNDCs(double x, double y, double xMax, double xMin, double yMax, double yMin)
        {
            double ndcx = (x - xMin) / (xMax - xMin);
            double ndcy = (y - yMin) / (yMax - yMin);
            double[] l = new double[2] { ndcx, ndcy };

            return l;
        }

        public int[] NdcsToDcs(double ndcx, double ndcy, double ndh, double ndv)
        {
            int dcx = (int)Math.Round(ndcx * (ndh - 1), 0);
            int dcy = (int)Math.Round(ndcy * (ndv - 1), 0);

            int[] l = new int[2] { dcx, dcy };

            return l;
        }

    }
}
