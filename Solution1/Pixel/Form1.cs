﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Pixel
{
    public partial class Form1 : Form
    {
        int x1, y1, opcao, x2, y2;
        public Form1(int x1, int y1, int opcao, int x2, int y2)
        {
            InitializeComponent();
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.opcao = opcao;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (opcao == 1)
            {
                int largura = 571, altura = 479;
                Bitmap bm = new Bitmap(largura, altura);
                pictureBox1.Image = bm;
                bm.SetPixel(x1, y1, Color.Red);
            }
            if (opcao == 2)
            {
                int largura = 571, altura = 479;
                Bitmap bm = new Bitmap(largura, altura);
                pictureBox1.Image = bm;
                inc_line(x1,y1,x2,y2,bm);
            }


        }

        public void inc_line(int x1, int y1, int x2, int y2, Bitmap bm)
        {
            int dx, dy, incE, incNE, d, x, y;
            dx = x2 - x1;
            dy = y2 - y1;
            d = 2 * (dy - dx);

            incE = 2 * dy;
            incNE = 2 * (dy - dx);
            x = x1;
            y = y1;
            bm.SetPixel(x, y, Color.Red);

            while(x < x2)
            {
                if (d <= 0)
                {
                    d = d + incE;
                    x = x + 1;
                }
                else
                {
                    d = d + incNE;
                    x = x + 1;
                    y = y + 1;
                }
                bm.SetPixel(x, y, Color.Red);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            

            
        }
    }
}
